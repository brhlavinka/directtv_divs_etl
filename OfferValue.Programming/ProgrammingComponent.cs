﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace OfferValue.Programming
{
    public class ProgrammingComponent
    {
        private IList<ProgrammingTerm> terms;
        private string offerId;
        private decimal cashValue = 0m;
        private decimal apValue = 0m;
        private decimal premiumValue = 0m;
        private decimal otherIncentivesValue = 0m;
        private decimal otherIncentivesGWPValue = 0m;
        private decimal otherIncentivesCBOValue = 0m;

        public ProgrammingComponent(SqlDataReader reader)
        {
            terms = new List<ProgrammingTerm>();

            offerId = reader["offerId"].ToString();
            decimal.TryParse(reader["miBundleCashValue"].ToString(), out cashValue);
            decimal.TryParse(reader["miBundleAPValue"].ToString(), out apValue);
            decimal.TryParse(reader["miBundlePremiumValue"].ToString(), out premiumValue);
            decimal.TryParse(reader["miBundleOtherIncentivesValue"].ToString(), out otherIncentivesValue);
            decimal.TryParse(reader["miBundleOtherIncentivesGWPValue"].ToString(), out otherIncentivesGWPValue);
            decimal.TryParse(reader["miBundleOtherIncentivesCBOValue"].ToString(), out otherIncentivesCBOValue);

            for (var i = 1; i < 7; i++)
            {
                var term = reader[string.Format(@"miBundlePromoTerm{0}", i)].ToString();
                var price = reader[string.Format(@"miBundlePromoPrice{0}", i)].ToString();

                var programmingTerm = new ProgrammingTerm(term, price);
                if (programmingTerm.HasData())
                {
                    terms.Add(programmingTerm);
                }
                else
                {
                    break;
                }
            }
        }

        public string GetOfferId()
        {
            return offerId;
        }
        public string GetComponentString()
        {
            return string.Join(", ", terms.Select(t => t.GetCompiledString())); 
        }
        public decimal GetComponentValue()
        {
            var total = 0m;
            decimal? rackRate = null;

            for (var i = terms.Count - 1; i >= 0; i--)
            {
                if (rackRate == null)
                {
                    rackRate = terms[i].GetPrice();
                }

                int? nextStartingMonth = null;
                if (i < (terms.Count - 1))
                {
                    nextStartingMonth = terms[i + 1].GetFirstMonth();
                }

                total += terms[i].GetValue(rackRate.Value, nextStartingMonth);
            }

            total = Decimal.Round(total, 2);

            return total;
        }
        public decimal GetTotalOfferValue()
        {
            return (cashValue > otherIncentivesCBOValue ? cashValue : otherIncentivesCBOValue)
                + apValue
                + premiumValue
                + otherIncentivesValue
                + otherIncentivesGWPValue
                + GetComponentValue();
        }
    }
}
