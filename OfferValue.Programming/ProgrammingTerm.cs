﻿using System;

namespace OfferValue.Programming
{
    public class ProgrammingTerm
    {
        private string term;
        private decimal price;

        public ProgrammingTerm(string term, string price)
        {
            this.term = term.Trim();
            this.term = this.term.Replace("'", "");
            this.term = this.term.Replace("NA", "");
            this.term = this.term.Replace("N/A", "");

            try
            {
                var trimmedPrice = price.Trim();
                if (trimmedPrice != string.Empty)
                {
                    this.price = decimal.Parse(price.Trim());
                }
                else
                {
                    this.price = 0m;
                }
            }
            catch (Exception ex)
            {
                this.price = 0m;
                Console.WriteLine(ex.Message);
            }
        }

        public decimal GetPrice()
        {
            return price;
        }
        public bool HasData()
        {
            return term != string.Empty;
        }
        public string GetCompiledString()
        {
            return String.Format(@"{1:C} / {0}", term, price);
        }
        public decimal GetValue(Decimal rackRate, int? nextStartingMonth)
        {
            var val = 0m;

            if (term.Contains("-"))
            {
                try
                {
                    var months = term.Split('-');
                    var firstMonth = int.Parse(months[0]);
                    var secondMonth = int.Parse(months[1]);

                    var termLength = (secondMonth - firstMonth) + 1;
                    val = termLength * (rackRate - price);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            else if (term.Contains("+") && nextStartingMonth.HasValue)
            {
                try
                {
                    var month = int.Parse(term.Replace("+", ""));
                    var termLength = nextStartingMonth.Value - month;

                    val = termLength * (rackRate - price);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return val;
        }
        public int? GetFirstMonth()
        {
            int? val = null;

            if (term.Contains("-"))
            {
                try
                {
                    var months = term.Split('-');
                    var firstMonth = int.Parse(months[0]);
                    val = firstMonth;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            else if (term.Contains("+"))
            {
                try
                {
                    var month = int.Parse(term.Replace("+", ""));
                    val = month;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return val;
        }
    }
}
