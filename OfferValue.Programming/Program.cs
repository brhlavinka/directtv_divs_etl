﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace OfferValue.Programming
{
    class Program
    {
        private static string CONNECTION_FORMAT =
            @"user id=pariveda;password=Pariveda1;server={0};Trusted_Connection=no;database={1}; connection timeout=30";

        static void Main(string[] args)
        {
            try
            {
                var server = @".";
                var database = @"directvci_dbo_dev";
                if (args.Length == 2)
                {
                    server = args[0];
                    database = args[1];
                }
                else
                {
                    Console.WriteLine(@"No server and/or database arguments provided, using default.");
                }

                Console.WriteLine(@"Connecting to database {0} at {1}...", database, server);

                var connectionString = string.Format(CONNECTION_FORMAT, server, database);
                using (SqlConnection myConnection = new SqlConnection(connectionString))
                {
                    Console.WriteLine(@"Opening connection...");
                    myConnection.Open();

                    var tableName = @"dbo.cimidashboardleadoffers";
                    SqlCommand comm = new SqlCommand(string.Format(@"SELECT * FROM {0}", tableName), myConnection);
                    Console.WriteLine(@"Reading all lead offers from {0}...", tableName);

                    var reader = comm.ExecuteReader();
                    Console.WriteLine(@"Compiling programming components for calculations...");
                    var offers = new List<ProgrammingComponent>();
                    while (reader.Read())
                    {
                        offers.Add(new ProgrammingComponent(reader));
                    }
                    reader.Close();
                    Console.WriteLine(@"Read a total of {0} lead offers...", offers.Count);
                    Console.WriteLine();

                    Console.WriteLine(@"Calculating lead offers values and updating the table...", offers.Count);
                    for (var i = 0; i < offers.Count; i++)
                    {
                        CalculateProgramming(offers[i], myConnection);
                        Console.Write("\r{0:P2}   ", (float)(i) / (float)(offers.Count));
                    }
                    Console.WriteLine(@"Finished calculations!");

                    myConnection.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(@"ETL failed for the following reason: {0}", ex.Message);
            }

            Console.WriteLine(@"Press enter to close...");
            Console.ReadLine();
        }

        private static void CalculateProgramming(ProgrammingComponent component, SqlConnection connection)
        {
            SqlCommand updateCommand = new SqlCommand(
                string.Format(@"UPDATE dbo.cimidashboardleadoffers SET miBundlePromoCompiled = '{0}', miBundlePromoCompiledValue = {1}, miBundleTotalOfferValue = {3} WHERE offerId = {2}",
                    component.GetComponentString(),
                    component.GetComponentValue(),
                    component.GetOfferId(),
                    component.GetTotalOfferValue()),
                connection);
            var rows = updateCommand.ExecuteNonQuery();
            if (rows != 1)
            {
                Console.WriteLine(component.GetOfferId());
                Console.WriteLine(string.Format(@"{0}: {1}", component.GetComponentValue(), component.GetComponentString()));
            }
        }
    }
}
