﻿use directvci_dbo_dev;

SELECT
	miZipCodeUsed,
	miDate,
	mappedMSO,
	miLeadOfferBundle,
	miBundleNameOfVideo,
	miBundlePromoPrice1,
	miBundlePromoTerm1,
	miBundleCash,
	miBundlePremium,
	miBundleOtherIncentives,
	miBundleAP,
	miBundleLevelHSD,
	miBundleLevelOfPhone,
	miBundleNumOfChannels,
	miBundlePromoCompiled,
	miBundlePromoCompiledValue,
	miBundleCashValue,
	miBundleAPValue,
	miBundlePremiumValue,
	miBundleOtherIncentivesValue,
	miBundleTotalOfferValue,
	miBundleOtherIncentivesGWPValue,
	miBundleOtherIncentivesCBOValue
FROM cimidashboardleadoffers
WHERE miBundleNameOfVideo IS NOT NULL;