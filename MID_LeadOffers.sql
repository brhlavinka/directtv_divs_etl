﻿use directvci_dbo_dev;

UPDATE dbo.cimidashboard SET mappedMSO = 'CHARTER' WHERE miMSO = 'Charter';
UPDATE dbo.cimidashboard SET mappedMSO = 'COMCAST' WHERE miMSO = 'Comcast';
UPDATE dbo.cimidashboard SET mappedMSO = 'COX' WHERE miMSO = 'Cox';
UPDATE dbo.cimidashboard SET mappedMSO = 'DISH' WHERE miMSO = 'DISH';
UPDATE dbo.cimidashboard SET mappedMSO = 'FIOS' WHERE miMSO = 'Verizon Fios';
UPDATE dbo.cimidashboard SET mappedMSO = 'TIMEWARNER' WHERE miMSO = 'Time Warner';
UPDATE dbo.cimidashboard SET mappedMSO = 'UVERSE' WHERE miMSO = 'AT&T U-verse';
UPDATE dbo.cimidashboard SET mappedMSO = 'BRIGHTHOUSE' WHERE miMSO = 'Brighthouse';
UPDATE dbo.cimidashboard SET mappedMSO = 'BUCKEYE' WHERE miMSO = 'Buckeye';
UPDATE dbo.cimidashboard SET mappedMSO = 'CABLEONE' WHERE miMSO = 'CableOne';
UPDATE dbo.cimidashboard SET mappedMSO = 'CABLEVISION' WHERE miMSO = 'Cablevision';
UPDATE dbo.cimidashboard SET mappedMSO = 'CENTURYLINK' WHERE miMSO = 'CenturyLink';
UPDATE dbo.cimidashboard SET mappedMSO = 'TIMEWARNER' WHERE miMSO = 'Insight';
UPDATE dbo.cimidashboard SET mappedMSO = 'MEDIACOM' WHERE miMSO = 'Mediacom';
UPDATE dbo.cimidashboard SET mappedMSO = 'MIDCONTINENT' WHERE miMSO = 'Midcontinent';
UPDATE dbo.cimidashboard SET mappedMSO = 'SUDDENLINK' WHERE miMSO = 'Suddenlink';
UPDATE dbo.cimidashboard SET mappedMSO = 'WOW' WHERE miMSO = 'Wide Open West';

SELECT DISTINCT
	miZipCodeUsed, miDate, mappedMSO, miLeadOfferBundle,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1NameOfVideo
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2NameOfVideo
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3NameOfVideo
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4NameOfVideo
		ELSE miBundle1NameOfVideo
	END as miBundleNameOfVideo,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1PromoPrice1
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2PromoPrice1
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3PromoPrice1
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4PromoPrice1
		ELSE miBundle1PromoPrice1
	END as miBundlePromoPrice1,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1PromoTerm1
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2PromoTerm1
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3PromoTerm1
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4PromoTerm1
		ELSE miBundle1PromoTerm1
	END as miBundlePromoTerm1,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1PromoPrice2
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2PromoPrice2
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3PromoPrice2
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4PromoPrice2
		ELSE miBundle1PromoPrice2
	END as miBundlePromoPrice2,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1PromoTerm2
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2PromoTerm2
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3PromoTerm2
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4PromoTerm2
		ELSE miBundle1PromoTerm2
	END as miBundlePromoTerm2,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1PromoPrice3
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2PromoPrice3
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3PromoPrice3
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4PromoPrice3
		ELSE miBundle1PromoPrice3
	END as miBundlePromoPrice3,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1PromoTerm3
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2PromoTerm3
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3PromoTerm3
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4PromoTerm3
		ELSE miBundle1PromoTerm3
	END as miBundlePromoTerm3,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1PromoPrice4
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2PromoPrice4
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3PromoPrice4
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4PromoPrice4
		ELSE miBundle1PromoPrice4
	END as miBundlePromoPrice4,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1PromoTerm4
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2PromoTerm4
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3PromoTerm4
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4PromoTerm4
		ELSE miBundle1PromoTerm4
	END as miBundlePromoTerm4,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1PromoPrice5
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2PromoPrice5
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3PromoPrice5
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4PromoPrice5
		ELSE miBundle1PromoPrice5
	END as miBundlePromoPrice5,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1PromoTerm5
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2PromoTerm5
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3PromoTerm5
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4PromoTerm5
		ELSE miBundle1PromoTerm5
	END as miBundlePromoTerm5,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1PromoPrice6
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2PromoPrice6
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3PromoPrice6
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4PromoPrice6
		ELSE miBundle1PromoPrice6
	END as miBundlePromoPrice6,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1PromoTerm6
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2PromoTerm6
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3PromoTerm6
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4PromoTerm6
		ELSE miBundle1PromoTerm6
	END as miBundlePromoTerm6,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1Cash
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2Cash
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3Cash
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4Cash
		ELSE miBundle1Cash
	END as miBundleCash,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1Premium
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2Premium
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3Premium
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4Premium
		ELSE miBundle1Premium
	END as miBundlePremium,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1OtherIncentives
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2OtherIncentives
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3OtherIncentives
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4OtherIncentives
		ELSE miBundle1OtherIncentives
	END as miBundleOtherIncentives,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1AP
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2AP
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3AP
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4AP
		ELSE miBundle1AP
	END as miBundleAP,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1LevelHSD
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2LevelHSD
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3LevelHSD
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4LevelHSD
		ELSE miBundle1LevelHSD
	END as miBundleLevelHSD,
	CASE WHEN miLeadOfferBundle = 'Good' THEN miBundle1LevelOfPhone
		WHEN miLeadOfferBundle = 'Better' THEN miBundle2LevelOfPhone
		WHEN miLeadOfferBundle = 'Best' THEN miBundle3LevelOfPhone
		WHEN miLeadOfferBundle = 'Alternate' THEN miBundle4LevelOfPhone
		ELSE miBundle1LevelOfPhone
	END as miBundleLevelOfPhone,
	CASE WHEN miLeadOfferBundle = 'Good' AND miBundle1NameOfVideo = miPackage1NameOfVideo THEN miPackage1NumOfChannels
		WHEN miLeadOfferBundle = 'Good' AND miBundle1NameOfVideo = miPackage2NameOfVideo THEN miPackage2NumOfChannels
		WHEN miLeadOfferBundle = 'Good' AND miBundle1NameOfVideo = miPackage3NameOfVideo THEN miPackage3NumOfChannels
		WHEN miLeadOfferBundle = 'Good' AND miBundle1NameOfVideo = miPackage4NameOfVideo THEN miPackage4NumOfChannels
		WHEN miLeadOfferBundle = 'Good' AND miBundle1NameOfVideo = miPackage5NameOfVideo THEN miPackage5NumOfChannels
		WHEN miLeadOfferBundle = 'Better' AND miBundle2NameOfVideo = miPackage1NameOfVideo THEN miPackage1NumOfChannels
		WHEN miLeadOfferBundle = 'Better' AND miBundle2NameOfVideo = miPackage2NameOfVideo THEN miPackage2NumOfChannels
		WHEN miLeadOfferBundle = 'Better' AND miBundle2NameOfVideo = miPackage3NameOfVideo THEN miPackage3NumOfChannels
		WHEN miLeadOfferBundle = 'Better' AND miBundle2NameOfVideo = miPackage4NameOfVideo THEN miPackage4NumOfChannels
		WHEN miLeadOfferBundle = 'Better' AND miBundle2NameOfVideo = miPackage5NameOfVideo THEN miPackage5NumOfChannels
		WHEN miLeadOfferBundle = 'Best' AND miBundle3NameOfVideo = miPackage1NameOfVideo THEN miPackage1NumOfChannels
		WHEN miLeadOfferBundle = 'Best' AND miBundle3NameOfVideo = miPackage2NameOfVideo THEN miPackage2NumOfChannels
		WHEN miLeadOfferBundle = 'Best' AND miBundle3NameOfVideo = miPackage3NameOfVideo THEN miPackage3NumOfChannels
		WHEN miLeadOfferBundle = 'Best' AND miBundle3NameOfVideo = miPackage4NameOfVideo THEN miPackage4NumOfChannels
		WHEN miLeadOfferBundle = 'Best' AND miBundle3NameOfVideo = miPackage5NameOfVideo THEN miPackage5NumOfChannels
		WHEN miLeadOfferBundle = 'Alternate' AND miBundle4NameOfVideo = miPackage1NameOfVideo THEN miPackage1NumOfChannels
		WHEN miLeadOfferBundle = 'Alternate' AND miBundle4NameOfVideo = miPackage2NameOfVideo THEN miPackage2NumOfChannels
		WHEN miLeadOfferBundle = 'Alternate' AND miBundle4NameOfVideo = miPackage3NameOfVideo THEN miPackage3NumOfChannels
		WHEN miLeadOfferBundle = 'Alternate' AND miBundle4NameOfVideo = miPackage4NameOfVideo THEN miPackage4NumOfChannels
		WHEN miLeadOfferBundle = 'Alternate' AND miBundle4NameOfVideo = miPackage5NameOfVideo THEN miPackage5NumOfChannels
		WHEN miBundle1NameOfVideo = miPackage1NameOfVideo THEN miPackage1NumOfChannels
		WHEN miBundle1NameOfVideo = miPackage2NameOfVideo THEN miPackage2NumOfChannels
		WHEN miBundle1NameOfVideo = miPackage3NameOfVideo THEN miPackage3NumOfChannels
		WHEN miBundle1NameOfVideo = miPackage4NameOfVideo THEN miPackage4NumOfChannels
		WHEN miBundle1NameOfVideo = miPackage5NameOfVideo THEN miPackage5NumOfChannels
	END as miBundleNumOfChannels,
	miMSO
INTO dbo.cimidashboardleadoffers
FROM dbo.cimidashboard
WHERE mappedMSO = 'CABLEVISION'
	OR mappedMSO = 'UVERSE'
	OR mappedMSO = 'CHARTER'
	OR mappedMSO = 'TIMEWARNER'
	OR mappedMSO = 'COMCAST'
	OR mappedMSO = 'FIOS'
	OR mappedMSO = 'COX'
ORDER BY miZipCodeUsed, mappedMSO, miDate;


INSERT INTO dbo.cimidashboardleadoffers
SELECT DISTINCT
	miZipCodeUsed, miDate, mappedMSO, miLeadOfferBundle,
	miPackage1NameOfVideo as miBundleNameOfVideo,
	miPackage1Promo1Price as miBundlePromoPrice1,
	miPackage1Promo1Term as miBundlePromoTerm1,
	miPackage1Promo2Price as miBundlePromoPrice2,
	miPackage1Promo2Term as miBundlePromoTerm2,
	miPackage1Promo3Price as miBundlePromoPrice3,
	miPackage1Promo3Term as miBundlePromoTerm3,
	miPackage1Promo4Price as miBundlePromoPrice4,
	miPackage1Promo4Term as miBundlePromoTerm4,
	miPackage1Promo5Price as miBundlePromoPrice5,
	miPackage1Promo5Term as miBundlePromoTerm5,
	miPackage1Promo6Price as miBundlePromoPrice6,
	miPackage1Promo6Term as miBundlePromoTerm6,
	miPackage1Cash as miBundleCash,
	miPackage1Premium as miBundlePremium,
	miPackage1OtherIncentives as miBundleOtherIncentives,
	miPackage1AP as miBundleAP,
	NULL as miBundleLevelHSD,
	NULL as miBundleLevelOfPhone,
	miPackage1NumOfChannels as miBundleNumOfChannels,
	miMSO
FROM dbo.cimidashboard
WHERE (mappedMSO = 'DISH'
	OR mappedMSO = 'DTV')
	AND miDMAID = 0
ORDER BY miZipCodeUsed, mappedMSO, miDate;

ALTER TABLE dbo.cimidashboardleadoffers
ADD miBundlePromoCompiled varchar(200);

ALTER TABLE dbo.cimidashboardleadoffers
ADD miBundlePromoCompiledValue float;

ALTER TABLE dbo.cimidashboardleadoffers
ADD miBundleCashValue smallmoney;

ALTER TABLE dbo.cimidashboardleadoffers
ADD miBundleAPValue smallmoney;

ALTER TABLE dbo.cimidashboardleadoffers
ADD miBundlePremiumValue smallmoney;

ALTER TABLE dbo.cimidashboardleadoffers
ADD miBundleOtherIncentivesValue smallmoney;

ALTER TABLE dbo.cimidashboardleadoffers
ADD miBundleOtherIncentivesGWPValue smallmoney;

ALTER TABLE dbo.cimidashboardleadoffers
ADD miBundleOtherIncentivesCBOValue smallmoney;

ALTER TABLE dbo.cimidashboardleadoffers
ADD miBundleTotalOfferValue smallmoney;

ALTER TABLE dbo.cimidashboardleadoffers
ADD offerId INT IDENTITY;

ALTER TABLE dbo.cimidashboardleadoffers
ADD CONSTRAINT PK_cimidashboardleadoffers
PRIMARY KEY(offerId);

UPDATE dbo.cimidashboardleadoffers
SET dbo.cimidashboardleadoffers.miBundleCashValue = MAPPING.value
FROM dbo.cimidashboardleadoffers OFFERS
INNER JOIN dbo.midleadoffercashmapping MAPPING
ON OFFERS.miMSO = MAPPING.mso AND OFFERS.miBundleCash = MAPPING.cash;

UPDATE dbo.cimidashboardleadoffers
SET dbo.cimidashboardleadoffers.miBundleAPValue = MAPPING.value
FROM dbo.cimidashboardleadoffers OFFERS
INNER JOIN dbo.midleadofferapmapping MAPPING
ON OFFERS.miMSO = MAPPING.mso AND OFFERS.miBundleAP = MAPPING.ap;

UPDATE dbo.cimidashboardleadoffers
SET dbo.cimidashboardleadoffers.miBundlePremiumValue = MAPPING.value
FROM dbo.cimidashboardleadoffers OFFERS
INNER JOIN dbo.midleadofferpremiummapping MAPPING
ON OFFERS.miMSO = MAPPING.mso AND OFFERS.miBundlePremium = MAPPING.premium;

UPDATE dbo.cimidashboardleadoffers
SET dbo.cimidashboardleadoffers.miBundleOtherIncentivesValue = MAPPING.oi_value,
	dbo.cimidashboardleadoffers.miBundleOtherIncentivesGWPValue = MAPPING.gwp_value,
	dbo.cimidashboardleadoffers.miBundleOtherIncentivesCBOValue = MAPPING.cbo_value
FROM dbo.cimidashboardleadoffers OFFERS
INNER JOIN dbo.midleadofferotherincentivesmapping MAPPING
ON OFFERS.miMSO = MAPPING.mso AND OFFERS.miBundleOtherIncentives = MAPPING.otherincentives;