﻿-- Dim_Location

locations = rank ${input1};
locations = foreach locations generate $0 as ID, ZIP, DMA_CODE, DMA_NAME;
${output1} = locations;

-- Dim_Date

customer_dates = foreach ${input2} generate YEAR, MONTH;
national_lead_offer_dates = foreach ${input8} generate YEAR, MONTH;
internet_speed_dates = foreach ${input9} generate FLATTEN(TOBAG(STRSPLIT(DATE, '/')));
internet_speed_dates = foreach internet_speed_dates generate $2 as YEAR, $0 as MONTH;
mid_lead_offer_dates = foreach ${input10} generate FLATTEN(TOBAG(STRSPLIT(DATE, '/')));
mid_lead_offer_dates = foreach mid_lead_offer_dates generate $2 as YEAR, $0 as MONTH;

dates = union customer_dates, national_lead_offer_dates;
dates = union dates, internet_speed_dates;
dates = union dates, mid_lead_offer_dates;
dates = distinct dates;
dates = order dates by YEAR asc, MONTH asc;
dates = rank dates;
dates = foreach dates generate $0 as ID, YEAR, MONTH;
${output2} = dates;

-- Dim_Package

packages = foreach ${input2} generate BASEPKG;
packages = distinct packages;
packages = order packages by BASEPKG asc;
packages = rank packages;
packages = foreach packages generate $0 as ID, BASEPKG as PACKAGE;
${output3} = packages;

-- Fact_Customer

customers = join locations by ZIP, ${input2} by ZIP;
customers = join dates by (YEAR, MONTH), customers by (YEAR, MONTH);
customers = join packages by PACKAGE, customers by BASEPKG;
${output4} = foreach customers generate locations::ID as LOCATION_ID, dates::ID as DATE_ID,
	packages::ID as PACKAGE_ID, VOL as VOLUNTARY_DISCONNECTS, INVOL as INVOLUNTARY_DISCONNECTS,
	BASE_CUST as CUSTOMER_COUNT;

-- Fact_Competitive

competitive = join locations by ZIP, ${input3} by ZIP;
${output5} = foreach competitive generate locations::ID as LOCATION_ID, PROVIDER;

-- Fact_Geocompetitive

geocompetitiveadditions = foreach ${input11} generate ZIP, SEGMENT;
geocompetitive = foreach ${input4} generate ZIP, SEGMENT;
geocompetitive = union geocompetitive, geocompetitiveadditions;
geocompetitive = join locations by ZIP, geocompetitive by ZIP;
${output6} = foreach geocompetitive generate locations::ID as LOCATION_ID, SEGMENT;

-- Fact_Demographic

demographic = join locations by ZIP, ${input5} by ZIP;
${output7} = foreach demographic generate locations::ID as LOCATION_ID, POPULATION, HOUSEHOLDS, AVG_HH_SIZE,
	AVG_HH_INCOME, AVG_AGE;

-- Dim_Provider

providers = ${input6};
providers = rank providers;
providers = foreach providers generate $0 as ID, NAME, TYPE, MARKET_SHARE_NAME, LEAD_OFFER_NAME;
${output8} = foreach providers generate ID, NAME, TYPE;

-- Fact_Market_Share

market_share = ${input7};
market_share = join providers by MARKET_SHARE_NAME left outer, market_share by PROVIDER;
market_share = join locations by ZIP, market_share by ZIP;
${output9} = foreach market_share generate locations::ID as LOCATION_ID, providers::ID as PROVIDER_ID,
	REPLACE(CUSTOMER_COUNT, '[^\\\\d\\\\.]', '')
	as CUSTOMER_COUNT;

-- Fact_National_Lead_Offer

national_lead_offer = ${input8};
national_lead_offer = join providers by LEAD_OFFER_NAME left outer, national_lead_offer by PROVIDER;
national_lead_offer = join dates by (YEAR, MONTH), national_lead_offer by (YEAR, MONTH);
${output10} = foreach national_lead_offer generate dates::ID as DATE_ID, providers::ID as PROVIDER_ID, OFFER,
	INTERNET_SPEED, REPLACE(PROGRAMMING, '[^\\\\d\\\\.]', '') as PROGRAMMING,
	REPLACE(CASH, '[^\\\\d\\\\.]', '') as CASH,
	REPLACE(GWP, '[^\\\\d\\\\.]', '') as GWP,
	REPLACE(AP_SVC, '[^\\\\d\\\\.]', '') as AP_SVC,
	REPLACE(PREMIUMS, '[^\\\\d\\\\.]', '') as PREMIUMS,
	REPLACE(OTHER, '[^\\\\d\\\\.]', '') as OTHER,
	REPLACE(LEAD_OFFER_VALUE, '[^\\\\d\\\\.]', '') as LEAD_OFFER_VALUE;

-- Internet_Speed

internet_speed = ${input9};
internet_speed = FOREACH internet_speed GENERATE ZIP, FLATTEN(TOBAG(STRSPLIT(DATE,'/'))),
	REPLACE(INTERNET_SPEED_1, '[^\\\\d\\\\.]', ''), REPLACE(INTERNET_SPEED_2, '[^\\\\d\\\\.]', ''),
	REPLACE(INTERNET_SPEED_3, '[^\\\\d\\\\.]', ''), REPLACE(INTERNET_SPEED_4, '[^\\\\d\\\\.]', ''),
	REPLACE(INTERNET_SPEED_5, '[^\\\\d\\\\.]', ''), REPLACE(INTERNET_SPEED_6, '[^\\\\d\\\\.]', ''),
	REPLACE(INTERNET_SPEED_7, '[^\\\\d\\\\.]', ''), REPLACE(INTERNET_SPEED_8, '[^\\\\d\\\\.]', '');
internet_speed = FOREACH internet_speed GENERATE $0 as ZIP, $3 as YEAR, $1 as MONTH,
	($4 == '' ? 0f : (float) $4), ($5 == '' ? 0f : (float) $5), ($6 == '' ? 0f : (float) $6),
	($7 == '' ? 0f : (float) $7), ($8 == '' ? 0f : (float) $8), ($9 == '' ? 0f : (float) $9),
	($10 == '' ? 0f : (float) $10), ($11 == '' ? 0f : (float) $11);
internet_speed = FOREACH internet_speed GENERATE ZIP, YEAR, MONTH,
	($3 > $4 ? $3 : $4), ($5 > $6 ? $5 : $6), ($7 > $8 ? $7 : $8), ($9 > $10 ? $9 : $10);
internet_speed = FOREACH internet_speed GENERATE ZIP, YEAR, MONTH,
	($3 > $4 ? $3 : $4), ($5 > $6 ? $5 : $6);
internet_speed = FOREACH internet_speed GENERATE ZIP, YEAR, MONTH,
	($3 > $4 ? $3 : $4) AS TOP_INTERNET_SPEED;
internet_speed = join locations by ZIP, internet_speed by ZIP;
internet_speed = join dates by (YEAR, MONTH), internet_speed by (YEAR, MONTH);
${output11} = foreach internet_speed generate locations::ID as LOCATION_ID, dates::ID as DATE_ID, TOP_INTERNET_SPEED;

-- Fact_Mid_Lead_Offer

mid_lead_offer = ${input10};
mid_lead_offer = FOREACH mid_lead_offer GENERATE ZIP, FLATTEN(TOBAG(STRSPLIT(DATE,'/'))), PROVIDER,
	LEVEL, NAME, PRICE, TERM, CASH, PREMIUM, OTHER_INCENTIVES, AP, INTERNET_SPEED, PHONE, CHANNEL_COUNT,
	PROMO_COMPILED, PROMO_VALUE, CASH_VALUE, AP_VALUE, PREMIUM_VALUE, OTHER_INCENTIVES_VALUE, TOTAL_OFFER_VALUE,
	OI_GWP_VALUE, OI_CBO_VALUE;
mid_lead_offer = FOREACH mid_lead_offer GENERATE $0 as ZIP, $3 as YEAR, $2 as DAY, $1 as MONTH,
	$4 as PROVIDER, $5 as LEVEL, $6 as NAME, $7 as PRICE, $8 as TERM, $9 as CASH, $10 as PREMIUM,
	$11 as OTHER_INCENTIVES, $12 as AP, $13 as INTERNET_SPEED, $14 as PHONE, $15 as CHANNEL_COUNT,
	$16 as PROMO_COMPILED, $17 as PROMO_VALUE, $18 as CASH_VALUE, $19 as AP_VALUE, $20 as PREMIUM_VALUE,
	$21 as OTHER_INCENTIVES_VALUE, $22 as TOTAL_OFFER_VALUE, $23 as OI_GWP_VALUE, $24 as OI_CBO_VALUE;
mid_lead_offer = join providers by NAME left outer, mid_lead_offer by PROVIDER;
mid_lead_offer = join locations by ZIP, mid_lead_offer by ZIP;
mid_lead_offer = join dates by (YEAR, MONTH), mid_lead_offer by (YEAR, MONTH);
${output12} = foreach mid_lead_offer generate
	locations::ID as LOCATION_ID,
	dates::ID as DATE_ID,
	providers::ID as PROVIDER_ID,
	DAY,
	LEVEL,
	mid_lead_offer::NAME,
	PRICE,
	REPLACE(TERM, '\\'', '') as TERM,
	REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CASH, '9999', ''), 'NULL', ''), 'N/A', ''), '6666', ''), '4444', '') as CASH,
	REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(PREMIUM, '9999', ''), 'NULL', ''), 'N/A', ''), '6666', ''), '4444', '') as PREMIUM,
	REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(OTHER_INCENTIVES, '9999', ''), 'NULL', ''), 'N/A', ''), '6666', ''), '4444', '') as OTHER_INCENTIVES,
	REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(AP, '9999', ''), 'NULL', ''), 'N/A', ''), '6666', ''), '4444', '') as AP,
	REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(INTERNET_SPEED, '9999', ''), 'NULL', ''), 'N/A', ''), '6666', ''), '4444', '') as INTERNET_SPEED,
	REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(PHONE, '9999', ''), 'NULL', ''), 'N/A', ''), '6666', ''), '4444', '') as PHONE,
	REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CHANNEL_COUNT, '9999', ''), 'NULL', ''), 'N/A', ''), '6666', ''), '4444', '') as CHANNEL_COUNT,
	PROMO_COMPILED,
	PROMO_VALUE,
	REPLACE(CASH_VALUE, 'NULL', '0') as CASH_VALUE,
	REPLACE(AP_VALUE, 'NULL', '0') as AP_VALUE,
	REPLACE(PREMIUM_VALUE, 'NULL', '0') as PREMIUM_VALUE,
	REPLACE(OTHER_INCENTIVES_VALUE, 'NULL', '0') as OTHER_INCENTIVES_VALUE,
	TOTAL_OFFER_VALUE,
	REPLACE(OI_GWP_VALUE, 'NULL', '0') as OI_GWP_VALUE,
	REPLACE(OI_CBO_VALUE, 'NULL', '0') as OI_CBO_VALUE;