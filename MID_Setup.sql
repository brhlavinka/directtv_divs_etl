﻿use directvci_dbo_dev;

ALTER TABLE dbo.cimidashboard
ADD mappedMSO varchar(50);

ALTER TABLE dbo.cimsofranchise
ADD mappedMSO varchar(50);

CREATE TABLE dbo.midleadoffercashmapping
(
	mso varchar(255),
	cash varchar(max),
	value smallmoney
);

CREATE TABLE dbo.midleadofferapmapping
(
	mso varchar(255),
	ap varchar(max),
	value smallmoney
);

CREATE TABLE dbo.midleadofferpremiummapping
(
	mso varchar(255),
	premium varchar(max),
	value smallmoney
);

CREATE TABLE dbo.midleadofferotherincentivesmapping
(
	mso varchar(255),
	otherincentives varchar(max),
	oi_value smallmoney,
	gwp_value smallmoney,
	cbo_value smallmoney
);