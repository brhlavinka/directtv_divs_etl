﻿use directvci_dbo_dev;

SELECT miMSO, miBundleCash, miBundleCashValue, COUNT(miBundleCash) as total
FROM cimidashboardleadoffers
WHERE miBundleCashValue IS NULL
GROUP BY miMSO, miBundleCash, miBundleCashValue
ORDER BY total DESC, miMSO, miBundleCash, miBundleCashValue;

SELECT miMSO, miBundleAP, miBundleAPValue, COUNT(miBundleAP) as total
FROM cimidashboardleadoffers
WHERE miBundleAPValue IS NULL
GROUP BY miMSO, miBundleAP, miBundleAPValue
ORDER BY total DESC, miMSO, miBundleAP, miBundleAPValue;

SELECT miMSO, miBundlePremium, miBundlePremiumValue, COUNT(miBundlePremium) as total
FROM cimidashboardleadoffers
WHERE miBundlePremiumValue IS NULL
GROUP BY miMSO, miBundlePremium, miBundlePremiumValue
ORDER BY total DESC, miMSO, miBundlePremium, miBundlePremiumValue;

SELECT miMSO, miBundleOtherIncentives, miBundleOtherIncentivesValue, COUNT(miBundleOtherIncentives) as total
FROM cimidashboardleadoffers
WHERE miBundleOtherIncentivesValue IS NULL
GROUP BY miMSO, miBundleOtherIncentives, miBundleOtherIncentivesValue
ORDER BY total DESC, miMSO, miBundleOtherIncentives, miBundleOtherIncentivesValue;

SELECT miMSO, miBundleCash, miBundleCashValue, miBundleOtherIncentives, miBundleOtherIncentivesCBOValue, COUNT(miBundleCash) as total
FROM cimidashboardleadoffers
WHERE miBundleCashValue IS NOT NULL
	AND miBundleCashValue <> 0
	AND miBundleOtherIncentivesCBOValue IS NOT NULL
	AND miBundleOtherIncentivesCBOValue <> 0
GROUP BY miMSO, miBundleCash, miBundleCashValue, miBundleOtherIncentives, miBundleOtherIncentivesCBOValue
ORDER BY total DESC, miMSO, miBundleCash, miBundleCashValue;